<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Producto;


/* @var $this yii\web\View */
/* @var $model app\models\Venta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="venta-form">

  <?php $form = ActiveForm::begin([
    'enableClientValidation' => false, //Evitar validaciones del lado del cliente
]); ?>
    <div class="row" style="margin-bottom:16px;">
      <div class="col-lg-8">
        <?= $form->field($model->venta, 'cliente')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-lg-4">
        <?= $form->field($model->venta, 'fecha')->widget(\yii\jui\DatePicker::classname(), [
			  'language' => 'es',
			  'dateFormat' => 'yyyy-MM-dd',
			  'options'=>['class'=>'form-control']
			]) ?>
      </div>
    </div>

  <?php
//Cargar un producto por defecto
 $producto = new Producto();
 $producto->loadDefaultValues();

  ?>
<!--Inicio sección de productos -->
 <div id="venta-productos">
   <div class="row" style="margin-bottom:16px;">
     <div class="col-lg-12">
      <?php
      //Boton para insertar nuevo formulario de producto
      echo Html::a('<span class="glyphicon glyphicon-plus" ></span> '.'Nuevo Producto', 'javascript:void(0);', [
         'id' => 'venta-nuevo-producto-boton',
         'class' => 'btn btn-success btn-md'
       ])
       ?>
  </div>
   </div>
  <!-- Cabeceras con etiquetas -->
  <div class="row">
    <div class="col-lg-3">
      <label class="control-label">
      <?= $producto->getAttributeLabel('cant') ?>
      </label>
    </div>
    <div class="col-lg-8">
      <label class="control-label">
      <?= $producto->getAttributeLabel('descripcion') ?>
      </label>
    </div>
    <div class="col-lg-1"></div>
  </div>
</div>


 <?php
 //Recorrer los productos
  foreach ($model->productos as $key => $_producto) {
    //Para cada producto renderizar el formulario de producto
    //Si el producto está vacío colocar 'nuevo' como clave, si no asignar el id del producto
    echo '<tr>';
    echo $this->render('_form-producto-venta', [
      'key' => $_producto->isNewRecord ? (strpos($key, 'nuevo') !== false ? $key : 'nuevo' . $key) : $_producto->productoId,
      'form' => $form,
      'producto' => $_producto,
    ]);
    echo '</tr>';
  }

//Producto vacío con su respectivo formulario que se utilizará para copiar cada vez que se presione el botón de nuevo producto
$producto = new Producto();
$producto->loadDefaultValues();
echo '<div id="venta-nuevo-producto-block" style="display:none">';
echo $this->render('_form-producto-venta', [
      'key' => '__id__',
      'form' => $form,
      'producto' => $producto,
  ]);
  echo '</div>';
  ?>

  <?php ob_start(); ?>

 <script>
      //Crear la clave para el producto
      var producto_k = <?php echo isset($key) ? str_replace('nuevo', '', $key) : 0; ?>;
      //Al hacer click en el boton de nuevo producto aumentar en uno la clave
      // y agregar un formulario de producto reemplazando la clave __id__ por la nueva clave
      $('#venta-nuevo-producto-boton').on('click', function () {
          producto_k += 1;
          $('#venta-productos').append($('#venta-nuevo-producto-block').html().replace(/__id__/g, 'nuevo' + producto_k));
        });

     //Al hacer click en un botón de eliminar eliminar la fila más cercana
     $(document).on('click', '.venta-eliminar-producto-boton', function () {
          $(this).closest('.row').remove();
      });

  </script>
  <?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>



    <?php ActiveForm::end(); ?>

</div>
