<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

   <div class="row producto">
   		<div class="col-lg-2">
   			<?= $form->field($producto, 'cant')->textInput([
    			'id' => "Productos_{$key}_cant",
        	'name' => "Productos[$key][cant]",
          'class' =>'form-control'
    		])->label(false) ?>
		</div>
    <div class="col-lg-9">
   			 <?= $form->field($producto, 'descripcion')->textInput(['maxlength' => true,'id' => "Productos_{$key}_descripcion",
        		'name' => "Productos[$key][descripcion]"])->label(false) ?>
		</div>
    	<div class="col-lg-1">
    	 	<?= Html::a('Eliminar' , 'javascript:void(0);', [
    	  'class' => 'venta-eliminar-producto-boton btn btn-danger',
   			 ]) ?>
    	</div>
    </div>
