<?php

/* @var $this yii\web\View */

$this->title = 'Formulario uno a muchos';
use yii\helpers\Url;
use \yii\helpers\Html
?>
<div class="site-index">


    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
            <h2>Formulario con relación uno-muchos</h2>
            <p> <a class="btn btn-primary btn-lg" href="<?php echo Url::to(['/venta']); ?>">Ver ejemplo</a></p>
          </div>
       </div>
       <div class="row">
           <div class="col-lg-6">
            <?php echo Html::img('@web/img/db.png') ?>
          </div>
          <div class="col-lg-6">
            <p>El siguiente ejemplo muestra como crear un formulario que permita la carga, actualización y eliminación
              de dos tablas con una relación de uno-a-muchos.
              En este caso se cuenta con dos tablas (Ventas y Productos), cada Venta puede tener asociada muchos Productos
            </p>
         </div>
      </div>


            </div>
        </div>
    </div>
</div>
