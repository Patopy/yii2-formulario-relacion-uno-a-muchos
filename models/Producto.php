<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $productoId Id
 * @property int $ventaId
 * @property string $descripcion Descripción
 * @property int $cant Cantidad
 *
 * @property Ventas $venta
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ventaId', 'descripcion', 'cant'], 'required'],
            [['ventaId', 'cant'], 'integer'],
            [['descripcion'], 'string', 'max' => 255],
            [['ventaId'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['ventaId' => 'ventaId']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'productoId' => 'Id',
            'ventaId' => 'Venta ID',
            'descripcion' => 'Descripción',
            'cant' => 'Cantidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVenta()
    {
        return $this->hasOne(Ventas::className(), ['ventaId' => 'ventaId']);
    }
}
