<h1 align="center">Formulario para relación uno a muchos</h1>
<br>
<p>
Este código es el ejemplo del artículo <a href="https://juncotic.com/yii2-formulario-para-relacion-uno-a-muchos">Yii2: Formulario para relación uno a muchos</a> de nuestro <a href="https://juncotic.com/blog/" >blog</a>.
</p>

<p>
En dicho artículo se explica como crear un formulario que permita manejar datos de dos tablas que tengan una relación de uno-a-muchos en Yii2.
</p>

